'use strict';
const bcrypt = require('bcrypt');
require('dotenv').config();

const saltRoundsEnv=process.env.SALTROUNDS_BCRYPT;
const saltRounds= parseInt(saltRoundsEnv)

const salt=bcrypt.genSaltSync(saltRounds);
const hash=bcrypt.hashSync("admin",salt);

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
      await queryInterface.bulkInsert('Users', [
        {
          username:"admin",
          password: hash,
          isAdmin:true,
          createdAt: new Date(),
          updatedAt: new Date()
      }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users', null, {});
  }
};
