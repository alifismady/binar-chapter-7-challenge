const express=require('express');
const route=express.Router();

const passport=require("../lib/passport")
const restrict= require('../middleware/restrict')

const AuthController = require('../controllers/AuthController')
const RoomController= require('../controllers/RoomController')
const GameController= require('../controllers/GameController')
const AdminController=require('../controllers/AdminController')

//ambil form register
route.get('/register', AuthController.userRenderRegister)
//register user baru
route.post('/register', AuthController.userRegister)

//ambil form login
route.get('/login', AuthController.userRenderLogin)
//authen login
route.post('/login',  AuthController.userLogin)
//check authen, kalo valid tampilkan roomPage
route.get('/create-room',restrict, RoomController.renderRoom)
//input roomname:string
route.post('/create-room', RoomController.createRoom)

//ini masih gagal, kalo di restrict terus pake async error req.next is not a function , kalo pake syncronus data nya masih pending
//masih bingung cara fixing nya, karena itu saya buat 2 endpoint bawah, 1 buat ngecek admin, 1 buat buka admin panel
route.get('/testauthenticate',restrict, AuthController.whoAmI)

//buat ngecek kalo isAdmin true/false dari restrict
route.get('/adminpanel',restrict, AdminController.renderAdminPanel)
//buat menampilkan adminpanel
route.get('/isitadmin',restrict, AdminController.checkAdmin)
//nampilkan adminpanel tanpa restrict
route.get('/adminpaneltanparestrict', AdminController.renderAdminPanelWithoutRestrict)


//authen dlu apakah user login, jika iya menampilkan ruangan sesuai parameter id
route.get('/fight/:room_id',restrict, GameController.renderFight)
//input roomid:int, player1:int, player1pick:'r'/'p'/'s', player2:int, player2pick:'r'/'p'/'s'
route.post('/fighting', GameController.fightProc)



// route.get('/failed', AuthController.sendFailed);
// route.get('/success', AuthController.send)

module.exports=route;