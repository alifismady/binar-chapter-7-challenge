const {User,Room,User_History}=require('../models')

class AdminController{

    static async renderAdminPanel(req,res){
            try{
                const currentUser=req.user;
                const isItAdmin=currentUser.isAdmin;
                const dataUser=await User.findAll({raw:true});
                const dataRoom=await Room.findAll({raw:true});
                const dataUserHistory= await User_History.findAll({raw:true});
                console.log(dataUser, '==> ini data user')
                res.render('adminPanel',{isItAdmin,dataUser,dataRoom,dataUserHistory})
            }catch(err){
                console.log(err)
            }
                
    }

    static checkAdmin(req,res){
        const currentUser=req.user;
        const isItAdmin=currentUser.isAdmin;
        res.render('adminChecker',{isItAdmin})
    }


    static async renderAdminPanelWithoutRestrict(req,res){
        try{
            // const currentUser=req.user;
            // const isItAdmin=currentUser.isAdmin;
            const dataUser=await User.findAll({raw:true});
            const dataRoom=await Room.findAll({raw:true});
            const dataUserHistory= await User_History.findAll({raw:true});
            console.log(dataUser, '==> ini data user')
            res.render('adminPanelTanpaRestrict',{dataUser,dataRoom,dataUserHistory})
        }catch(err){
            console.log(err)
        }
            
}
    
}

module.exports=AdminController