const {Room}= require('../models')




class RoomController{
    static renderRoom(req,res){
        res.render('roomPage');
    }

    static async createRoom(req,res){
        const{roomname}=req.body;

        const inputRoom= await Room.create({
            roomname:roomname
        })

        console.log("room berhasil dibuat");

        const getRoomID= await Room.findOne({
            where:{
                roomname:roomname
            }
        })
        res.send("room berhasil dibuat dengan ID:"+getRoomID.id)



    }
}

module.exports=RoomController;