const {Room,User_History}= require('../models')


class GameController{

    static async renderFight(req,res){
        try{
            console.log("room id dari params adalah:",req.params['room_id']);
            const roomUser=req.params['room_id']

            const getRoom=await Room.findOne({
                where:{
                    id:roomUser
                }
            })
            if(!getRoom) {
                console.log("room tidak ditemukan")
                res.redirect('/create-room')
            }else{
                console.log("room ditemukan:", getRoom.id)
                res.render('fightPage',{getRoom})
            }


        }catch(err){
            console.log(err)
        }
        
        
    }

    static async gameRPS(req,res,roomid,p1,p1p,p2,p2p){
        try{
            if(p1p==p2p){
                    console.log("Draw");
                    res.redirect('/fight/'+roomid)
            }else if(p1p=="r"){
                if(p2p=="p"){
                    console.log('player 2 win this round');
                    GameController.updateScoreRoom(req,res,roomid,2,p1,p2)
                }else if(p2p=='s'){
                    console.log('player 1 win this round');
                    GameController.updateScoreRoom(req,res,roomid,1,p1,p2)
                }else {
                    console.log("input player 2 tidak valid");
                    res.redirect('/fight/'+roomid)
                }
            }else if(p1p=='p'){
                if(p2p=="s"){
                    console.log('player 2 win this round');
                    GameController.updateScoreRoom(req,res,roomid,2,p1,p2)
                }else if(p2p=='r'){
                    console.log('player 1 win this round');
                    GameController.updateScoreRoom(req,res,roomid,1,p1,p2)
                }else {
                    console.log("input player 2 tidak valid");
                    res.redirect('/fight/'+roomid)
                }
            }else if(p1p=='s'){
                if(p2p=="r"){
                    console.log('player 2 win this round');
                    GameController.updateScoreRoom(req,res,roomid,2,p1,p2)
                }else if(p2p=='p'){
                    console.log('player 1 win this round');
                    GameController.updateScoreRoom(req,res,roomid,1,p1,p2)
                }else {
                    console.log("input player 2 tidak valid");
                    res.redirect('/fight/'+roomid)
                }
            }else{
                console.log('input player 1 tidak valid');
                res.redirect('/fight/'+roomid)
            }
        }catch(err){
            console.log(err)
        }
        
    }

    static async updateScoreRoom(req,res,roomid, whowin,player1,player2){
        try{
            const getScore = await Room.findOne({
                where:{
                    id:roomid
                }
            })
            console.log("score player 1 sebelum update:",getScore.scorePlayer1)
            console.log("score player 2 sebelum update:",getScore.scorePlayer2)

            if(whowin==1){
                const newScore=getScore.scorePlayer1+1;
                const updateScore= await Room.update(
                    {scorePlayer1:newScore},
                    {where:{id:roomid}}
                )
                console.log('skor tertinggi sekarang:',newScore)
                if(newScore==3){
                    console.log('reset score trigger')
                    const resetScore= await Room.update(
                        {scorePlayer1:0,scorePlayer2:0},
                        {where:{id:roomid}}
                    )
                    GameController.updateHistoryScore(player1)
                    res.send("Player 1 win the game")
                }else{
                    console.log('game belum selesai redirect ke room fight')
                    res.redirect('/fight/'+roomid);
            }
            }else if(whowin==2){
                const newScore=getScore.scorePlayer2+1;
                const updateScore= await Room.update(
                    {scorePlayer2:newScore},
                    {where:{id:roomid}}
                )
                console.log('skor tertinggi sekarang:',newScore)
                if(newScore==3){
                    console.log('reset score trigger')
                    const resetScore= await Room.update(
                        {scorePlayer1:0,scorePlayer2:0},
                        {where:{id:roomid}}
                    )
                    GameController.updateHistoryScore(player2)
                    res.send("Player 2 win the game")
                }else{
                    console.log('game belum selesai redirect ke room fight')
                    res.redirect('/fight/'+roomid);
            }  
            }
            

            


        }catch(err){
            console.log(err)
        }

    }

    static async updateHistoryScore(idPlayer){
        try{
            const cekPlayerHistory= await User_History.findOne({
                where:{idPlayer:idPlayer}
            })
            if(!cekPlayerHistory){
                console.log('user history tidak ditemukan, masuk create user history')
                const createNewUserHistory= await User_History.create({
                    idPlayer:idPlayer
                })
                console.log("user history telah dibuat")
                GameController.updateHistoryScore(idPlayer)
            }
            else{
                console.log('User history ditemukan, dilakukan penambahan score')
                const currentWinnerScore=cekPlayerHistory.currentScore+1;
                await cekPlayerHistory.update({currentScore:currentWinnerScore})
                console.log("user dengan id:",cekPlayerHistory.idPlayer, "memiliki score:",cekPlayerHistory.currentScore)
                
            }

        }catch(err){
            console.log(err)
        }

    }
    static async fightProc(req,res){
        try{
            const {roomid,player1,player1pick,player2,player2pick} = req.body;
            GameController.gameRPS(req,res,roomid,player1,player1pick,player2,player2pick);


        }catch(err){
            console.log(err)
        }
    }
}



module.exports=GameController